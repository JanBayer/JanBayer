# Jan Bayer

Hi there! I'm a Computer Science student working as a Web Developer in a Working Student relationship at [Auctores GmbH](https://auctores.de).

This is my GitLab account specifically for work. To see what else I've made, please check out [my GitHub account](https://github.com/Erdragh) or [my personal GitLab account](https://gitlab.com/Erdragh).
